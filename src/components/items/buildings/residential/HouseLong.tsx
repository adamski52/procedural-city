import BaseItem from "../../BaseItem";
import img from "../../../../img/house-long.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class HouseLong extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 84,
                depth: 75
            },
            spriteSizeCorrection: {
                width: 10,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.RESIDENTIAL,
                TagsEnum.HOUSE
            ]
        });
    }
}