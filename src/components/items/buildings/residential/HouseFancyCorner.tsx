import BaseItem from "../../BaseItem";
import img from "../../../../img/house-fancy-corner.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class HouseFancyCorner extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 103,
                depth: 104
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.RESIDENTIAL,
                TagsEnum.HOUSE
            ]
        });
    }
}