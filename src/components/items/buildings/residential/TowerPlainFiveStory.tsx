import BaseItem from "../../BaseItem";
import img from "../../../../img/tower-plain-5-story.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class TowerPlainFiveStory extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 106,
                depth: 160
            },
            spriteSizeCorrection: {
                width: 21,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 3,
                width: 2
            },
            tags: [
                TagsEnum.RESIDENTIAL,
                TagsEnum.APARTMENT
            ]
        });
    }
}