import BaseItem from "../../BaseItem";
import img from "../../../../img/tower-fancy-tall.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class TowerFancyTall extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 92,
                depth: 224
            },
            spriteSizeCorrection: {
                width: 10,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 3,
                width: 2
            },
            tags: [
                TagsEnum.RESIDENTIAL,
                TagsEnum.APARTMENT,
                TagsEnum.MEDIUM
            ]
        });
    }
}