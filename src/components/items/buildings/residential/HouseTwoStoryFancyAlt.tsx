import BaseItem from "../../BaseItem";
import img from "../../../../img/house-2-story-fancy-alt.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class HouseTwoStoryFancyAlt extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 93,
                depth: 123
            },
            spriteSizeCorrection: {
                width: -12,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.RESIDENTIAL,
                TagsEnum.HOUSE,
                TagsEnum.ALTERNATE
            ]
        });
    }
}