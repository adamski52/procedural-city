import BaseItem from "../../BaseItem";
import img from "../../../../img/tower-fancy-short.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class TowerFancyShort extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 94,
                depth: 144
            },
            spriteSizeCorrection: {
                width: 8,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 3,
                width: 2
            },
            tags: [
                TagsEnum.RESIDENTIAL,
                TagsEnum.APARTMENT,
                TagsEnum.SMALL
            ]
        });
    }
}