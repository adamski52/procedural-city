import BaseItem from "../../BaseItem";
import img from "../../../../img/house-fancy-1-story.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class HouseFancyOneStory extends BaseItem {
    constructor() {
        super({
            spriteSize: { width: 94, depth: 96 }, spriteSizeCorrection: { width: -13, depth: -16 },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.RESIDENTIAL,
                TagsEnum.HOUSE
            ]
        });
    }
}