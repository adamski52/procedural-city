import BaseItem from "../../BaseItem";
import img from "../../../../img/tower-large.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class TowerLarge extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 119,
                depth: 252
            },
            spriteSizeCorrection: {
                width: 14,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 4,
                width: 2
            },
            tags: [
                TagsEnum.RESIDENTIAL,
                TagsEnum.APARTMENT,
                TagsEnum.LARGE
            ]
        });
    }
}