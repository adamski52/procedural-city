import BaseItem from "../../BaseItem";
import img from "../../../../img/house-corner.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class HouseCorner extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 106,
                depth: 79
            },
            spriteSizeCorrection: {
                width: -7,
                depth: -23
            },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.RESIDENTIAL,
                TagsEnum.HOUSE
            ]
        });
    }
}