import BaseItem from "../../BaseItem";
import img from "../../../../img/house-2-story.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class HouseTwoStory extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 83,
                depth: 96
            },
            spriteSizeCorrection: {
                width: 9,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.RESIDENTIAL,
                TagsEnum.HOUSE
            ]
        });
    }
}