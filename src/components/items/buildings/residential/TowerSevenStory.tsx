import BaseItem from "../../BaseItem";
import img from "../../../../img/tower-7-story.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class TowerSevenStory extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 119,
                depth: 236
            },
            spriteSizeCorrection: {
                width: -12,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 2,
                width: 3
            },
            tags: [
                TagsEnum.RESIDENTIAL,
                TagsEnum.APARTMENT
            ]
        });
    }
}