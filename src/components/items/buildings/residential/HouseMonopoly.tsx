import BaseItem from "../../BaseItem";
import img from "../../../../img/house-monopoly.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class HouseMonopoly extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 69,
                depth: 64
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.RESIDENTIAL,
                TagsEnum.HOUSE
            ]
        });
    }
}