import BaseItem from "../../BaseItem";
import img from "../../../../img/tower-small-plain.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class TowerSmallPlain extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 140,
                depth: 151
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -17
            },
            img,
            isometricSize: {
                depth: 3,
                width: 3
            },
            tags: [
                TagsEnum.RESIDENTIAL,
                TagsEnum.APARTMENT,
                TagsEnum.SMALL
            ]
        });
    }
}