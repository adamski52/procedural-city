import BaseItem from "../../BaseItem";
import img from "../../../../img/plane-small-away.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class PlaneSmallAway extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 103,
                depth: 55
            },
            spriteSizeCorrection: {
                width: -9,
                depth: -30
            },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.VEHICLE,
                TagsEnum.AIRPORT,
                TagsEnum.AIRPLANE,
                TagsEnum.SMALL,
                TagsEnum.AWAY
            ]
        });
    }
}