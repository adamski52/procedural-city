import BaseItem from "../../BaseItem";
import img from "../../../../img/plane-large-toward.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class PlaneLargeToward extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 138,
                depth: 83
            },
            spriteSizeCorrection: {
                width: -12,
                depth: -50
            },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.VEHICLE,
                TagsEnum.AIRPORT,
                TagsEnum.AIRPLANE,
                TagsEnum.LARGE,
                TagsEnum.FACING
            ]
        });
    }
}