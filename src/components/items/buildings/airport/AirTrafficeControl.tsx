import BaseItem from "../../BaseItem";
import img from "../../../../img/atc.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class AirTrafficControl extends BaseItem {
    constructor() {
        super({
            spriteSize: { width: 99, depth: 192 },
            spriteSizeCorrection: { width: 1.5, depth: -14 },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.AIRPORT
            ]
        });
    }
}