import BaseItem from "../../BaseItem";
import img from "../../../../img/hangar-large.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class HangarLarge extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 243,
                depth: 185
            },
            spriteSizeCorrection: {
                width: 20,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 5,
                width: 4
            },
            tags: [
                TagsEnum.AIRPORT,
                TagsEnum.HANGAR,
                TagsEnum.FACING,
                TagsEnum.LARGE
            ]
        });
    }
}