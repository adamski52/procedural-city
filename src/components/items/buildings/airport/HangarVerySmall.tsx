import BaseItem from "../../BaseItem";
import img from "../../../../img/hangar-very-small.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class HangarVerySmall extends BaseItem {
    constructor() {
        super({
            spriteSize: { width: 77, depth: 66 }, spriteSizeCorrection: { width: -3, depth: -16 },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.AIRPORT,
                TagsEnum.HANGAR,
                TagsEnum.FACING,
                TagsEnum.SMALL
            ]
        });
    }
}