import BaseItem from "../../BaseItem";
import img from "../../../../img/hangar.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class Hangar extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 174,
                depth: 136
            },
            spriteSizeCorrection: {
                width: -14,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 3,
                width: 4
            },
            tags: [
                TagsEnum.AIRPORT,
                TagsEnum.HANGAR,
                TagsEnum.FACING
            ]
        });
    }
}