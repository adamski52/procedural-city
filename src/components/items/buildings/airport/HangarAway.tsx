import BaseItem from "../../BaseItem";
import img from "../../../../img/hangar-away.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class HangarAway extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 174,
                depth: 136
            },
            spriteSizeCorrection: {
                width: -13,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 4,
                width: 4
            },
            tags: [
                TagsEnum.AIRPORT,
                TagsEnum.HANGAR,
                TagsEnum.AWAY
            ]
        });
    }
}