import BaseItem from "../../BaseItem";
import img from "../../../../img/runway.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class Runway extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 461,
                depth: 265
            },
            spriteSizeCorrection: {
                width: 147,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 20,
                width: 5
            },
            tags: [
                TagsEnum.AIRPORT,
                TagsEnum.RUNWAY
            ]
        });
    }
}