import BaseItem from "../../BaseItem";
import img from "../../../../img/factory-1-story-long.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class FactoryOneStoryLong extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 160,
                depth: 140
            },
            spriteSizeCorrection: {
                width: -31,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 2,
                width: 5
            },
            tags: [
                TagsEnum.INDUSTRIAL,
                TagsEnum.FACTORY
            ]
        });
    }
}