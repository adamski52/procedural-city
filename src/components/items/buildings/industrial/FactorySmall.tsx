import BaseItem from "../../BaseItem";
import img from "../../../../img/factory-small.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class FactorySmall extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 84,
                depth: 91
            },
            spriteSizeCorrection: {
                width: 7,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.INDUSTRIAL,
                TagsEnum.FACTORY,
                TagsEnum.SMALL
            ]
        });
    }
}