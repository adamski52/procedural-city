import BaseItem from "../../BaseItem";
import img from "../../../../img/factory-1-stack.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class FactoryOneStack extends BaseItem {
    constructor() {
        super({
            spriteSize: { width: 76, depth: 156 }, spriteSizeCorrection: { width: 0.5, depth: -15 },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.INDUSTRIAL,
                TagsEnum.FACTORY
            ]
        });
    }
}