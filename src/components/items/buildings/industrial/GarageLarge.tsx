import BaseItem from "../../BaseItem";
import img from "../../../../img/garage-large.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class GarageLarge extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 152,
                depth: 154
            },
            spriteSizeCorrection: {
                width: 28,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 5,
                width: 2
            },
            tags: [
                TagsEnum.INDUSTRIAL,
                TagsEnum.GARAGE,
                TagsEnum.LARGE
            ]
        });
    }
}