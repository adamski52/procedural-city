import BaseItem from "../../BaseItem";
import img from "../../../../img/factory-very-large.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class FactoryVeryLarge extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 196,
                depth: 257
            },
            spriteSizeCorrection: {
                width: 8,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 5,
                width: 5
            },
            tags: [
                TagsEnum.INDUSTRIAL,
                TagsEnum.FACTORY,
                TagsEnum.XLARGE
            ]
        });
    }
}