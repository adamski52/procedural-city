import BaseItem from "../../BaseItem";
import img from "../../../../img/garage-small.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class GarageSmall extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 89,
                depth: 81
            },
            spriteSizeCorrection: {
                width: -2,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.INDUSTRIAL,
                TagsEnum.GARAGE,
                TagsEnum.SMALL
            ]
        });
    }
}