import BaseItem from "../../BaseItem";
import img from "../../../../img/factory-4-story-2-stack.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class FactoryFourStoryTwoStack extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 106,
                depth: 194
            },
            spriteSizeCorrection: {
                width: 22,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 4,
                width: 2
            },
            tags: [
                TagsEnum.INDUSTRIAL,
                TagsEnum.FACTORY
            ]
        });
    }
}