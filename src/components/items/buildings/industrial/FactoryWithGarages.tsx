import BaseItem from "../../BaseItem";
import img from "../../../../img/factory-with-garages.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class FactoryWithGarages extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 194,
                depth: 200
            },
            spriteSizeCorrection: {
                width: -24,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 4,
                width: 6
            },
            tags: [
                TagsEnum.INDUSTRIAL,
                TagsEnum.FACTORY,
                TagsEnum.GARAGE
            ]
        });
    }
}