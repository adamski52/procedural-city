import BaseItem from "../../BaseItem";
import img from "../../../../img/factory-small-tall.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class FactorySmallTall extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 76,
                depth: 226
            },
            spriteSizeCorrection: {
                width: 2,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.INDUSTRIAL,
                TagsEnum.FACTORY
            ]
        });
    }
}