import BaseItem from "../../BaseItem";
import img from "../../../../img/crane-yellow-large.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class CraneYellowLarge extends BaseItem {
    constructor() {
        super({
            spriteSize: { width: 189, depth: 355 }, spriteSizeCorrection: { width: 16.5, depth: -15 },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.INDUSTRIAL,
                TagsEnum.CRANE,
                TagsEnum.ALTERNATE
            ]
        });
    }
}