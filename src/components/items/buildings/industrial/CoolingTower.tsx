import BaseItem from "../../BaseItem";
import img from "../../../../img/cooling-tower.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class CoolingTower extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 76,
                depth: 120
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -26
            },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.INDUSTRIAL,
                TagsEnum.TOWER
            ]
        });
    }
}