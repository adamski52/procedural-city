import BaseItem from "../../BaseItem";
import img from "../../../../img/factory-1-story-3-stacks.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class FactoryOneStoryThreeStack extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 155,
                depth: 153
            },
            spriteSizeCorrection: {
                width: -21,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 2,
                width: 4
            },
            tags: [
                TagsEnum.INDUSTRIAL,
                TagsEnum.FACTORY
            ]
        });
    }
}