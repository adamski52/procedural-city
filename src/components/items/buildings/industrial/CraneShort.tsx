import BaseItem from "../../BaseItem";
import img from "../../../../img/crane-short.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class CraneShort extends BaseItem {
    constructor() {
        super({
            spriteSize: { width: 182, depth: 220 }, spriteSizeCorrection: { width: -15.5, depth: -15 },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.INDUSTRIAL,
                TagsEnum.CRANE,
                TagsEnum.SMALL
            ]
        });
    }
}