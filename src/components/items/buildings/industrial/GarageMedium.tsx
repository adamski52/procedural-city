import BaseItem from "../../BaseItem";
import img from "../../../../img/garage-medium.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class GarageMedium extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 103,
                depth: 119
            },
            spriteSizeCorrection: {
                width: 4,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.INDUSTRIAL,
                TagsEnum.GARAGE,
                TagsEnum.MEDIUM
            ]
        });
    }
}