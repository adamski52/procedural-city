import BaseItem from "../../BaseItem";
import img from "../../../../img/office-2-story-corner.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class OfficeTwoStoryCorner extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 101,
                depth: 106
            },
            spriteSizeCorrection: {
                width: 8,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 3,
                width: 2
            },
            tags: [
                TagsEnum.COMMERCIAL,
                TagsEnum.OFFICE
            ]
        });
    }
}