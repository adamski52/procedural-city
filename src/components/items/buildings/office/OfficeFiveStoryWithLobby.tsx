import BaseItem from "../../BaseItem";
import img from "../../../../img/office-5-story-with-lobby.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class OfficeFiveStoryWithLobby extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 79,
                depth: 176
            },
            spriteSizeCorrection: {
                width: 2,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 3,
                width: 3
            },
            tags: [
                TagsEnum.COMMERCIAL,
                TagsEnum.OFFICE,
                TagsEnum.TOWER
            ]
        });
    }
}