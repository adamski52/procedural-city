import BaseItem from "../../BaseItem";
import img from "../../../../img/office-glass-medium.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class OfficeGlassMedium extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 129,
                depth: 252
            },
            spriteSizeCorrection: {
                width: -10,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 2,
                width: 4
            },
            tags: [
                TagsEnum.COMMERCIAL,
                TagsEnum.OFFICE,
                TagsEnum.TOWER,
                TagsEnum.MEDIUM
            ]
        });
    }
}