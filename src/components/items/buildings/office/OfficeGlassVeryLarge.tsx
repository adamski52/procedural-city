import BaseItem from "../../BaseItem";
import img from "../../../../img/office-glass-very-large.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class OfficeGlassVeryLarge extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 145,
                depth: 328
            },
            spriteSizeCorrection: {
                width: -4,
                depth: -18
            },
            img,
            isometricSize: {
                depth: 3,
                width: 3
            },
            tags: [
                TagsEnum.COMMERCIAL,
                TagsEnum.OFFICE,
                TagsEnum.XLARGE
            ]
        });
    }
}