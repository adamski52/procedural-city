import BaseItem from "../../BaseItem";
import img from "../../../../img/office-12-story.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class OfficeTwelveStory extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 111,
                depth: 233
            },
            spriteSizeCorrection: {
                width: -4,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 3,
                width: 3
            },
            tags: [
                TagsEnum.COMMERCIAL,
                TagsEnum.TOWER
            ]
        });
    }
}