import BaseItem from "../../BaseItem";
import img from "../../../../img/office-glass-6-story.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class OfficeGlassSixStory extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 135,
                depth: 198
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 3,
                width: 3
            },
            tags: [
                TagsEnum.COMMERCIAL,
                TagsEnum.TOWER
            ]
        });
    }
}