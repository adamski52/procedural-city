import BaseItem from "../../BaseItem";
import img from "../../../../img/office-glass-large.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class OfficeGlassLarge extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 120,
                depth: 308
            },
            spriteSizeCorrection: {
                width: 15,
                depth: -14
            },
            img,
            isometricSize: {
                depth: 4,
                width: 2
            },
            tags: [
                TagsEnum.COMMERCIAL,
                TagsEnum.OFFICE,
                TagsEnum.TOWER,
                TagsEnum.LARGE
            ]
        });
    }
}