import BaseItem from "../../BaseItem";
import img from "../../../../img/construction-tower-medium.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class ConstructionTowerMedium extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 105,
                depth: 138
            },
            spriteSizeCorrection: {
                width: -11,
                depth: -18
            },
            img,
            isometricSize: {
                depth: 4,
                width: 4
            },
            tags: [
                TagsEnum.COMMERCIAL,
                TagsEnum.TOWER,
                TagsEnum.MEDIUM,
                TagsEnum.CONSTRUCTION
            ]
        });
    }
}