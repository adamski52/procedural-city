import BaseItem from "../../BaseItem";
import img from "../../../../img/office-long-windows-4-story.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class OfficeLongWindowsFourStory extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 127,
                depth: 146
            },
            spriteSizeCorrection: {
                width: -11,
                depth: -18
            },
            img,
            isometricSize: {
                depth: 2,
                width: 4
            },
            tags: [
                TagsEnum.COMMERCIAL,
                TagsEnum.OFFICE
            ]
        });
    }
}