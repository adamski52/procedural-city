import BaseItem from "../../BaseItem";
import img from "../../../../img/office-complex-fancy.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class OfficeComplexFancy extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 377,
                depth: 243
            },
            spriteSizeCorrection: {
                width: -9,
                depth: -90
            },
            img,
            isometricSize: {
                depth: 10,
                width: 10
            },
            tags: [
                TagsEnum.COMMERCIAL,
                TagsEnum.OFFICE,
                TagsEnum.XLARGE
            ]
        });
    }
}