import BaseItem from "../../BaseItem";
import img from "../../../../img/office-3-story.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class OfficeThreeStory extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 124,
                depth: 128
            },
            spriteSizeCorrection: {
                width: 5,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 3,
                width: 3
            },
            tags: [
                TagsEnum.COMMERCIAL,
                TagsEnum.OFFICE
            ]
        });
    }
}