import BaseItem from "../../BaseItem";
import img from "../../../../img/office-2-story-short.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class OfficeTwoStoryShort extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 121,
                depth: 105
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 3,
                width: 3
            },
            tags: [
                TagsEnum.COMMERCIAL,
                TagsEnum.OFFICE
            ]
        });
    }
}