import BaseItem from "../../BaseItem";
import img from "../../../../img/office-6-story-skinny.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class OfficeSixStorySkinny extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 88,
                depth: 225
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 3,
                width: 3
            },
            tags: [
                TagsEnum.COMMERCIAL,
                TagsEnum.TOWER
            ]
        });
    }
}