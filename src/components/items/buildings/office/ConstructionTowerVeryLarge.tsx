import BaseItem from "../../BaseItem";
import img from "../../../../img/construction-tower-very-large.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class ConstructionTowerVeryLarge extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 130,
                depth: 399
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -17
            },
            img,
            isometricSize: {
                depth: 4,
                width: 4
            },
            tags: [
                TagsEnum.COMMERCIAL,
                TagsEnum.TOWER,
                TagsEnum.XLARGE,
                TagsEnum.CONSTRUCTION
            ]
        });
    }
}