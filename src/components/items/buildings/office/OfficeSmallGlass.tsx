import BaseItem from "../../BaseItem";
import img from "../../../../img/office-small-glass.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class OfficeSmallGlass extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 107,
                depth: 100
            },
            spriteSizeCorrection: {
                width: 7,
                depth: -18
            },
            img,
            isometricSize: {
                depth: 3,
                width: 2
            },
            tags: [
                TagsEnum.COMMERCIAL,
                TagsEnum.TOWER
            ]
        });
    }
}