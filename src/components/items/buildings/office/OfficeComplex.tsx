import BaseItem from "../../BaseItem";
import img from "../../../../img/office-complex.png";
import { TagsEnum } from "../../../../interfaces/TagsEnum";

export default class OfficeComplex extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 204,
                depth: 170
            },
            spriteSizeCorrection: {
                width: 30,
                depth: -24
            },
            img,
            isometricSize: {
                depth: 5,
                width: 3
            },
            tags: [
                TagsEnum.COMMERCIAL,
                TagsEnum.OFFICE,
                TagsEnum.LARGE
            ]
        });
    }
}