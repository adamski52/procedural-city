import BasePlot from "./BasePlot";
import img from "../../../img/cement.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";
import BaseRoad from "../roads/BaseRoad";

export default class CementPlot extends BasePlot {
    constructor(road:BaseRoad) {
        super({
            road,
            img,
            tags: [
                TagsEnum.PLOT,
                TagsEnum.CONCRETE
            ]
        });
    }
}