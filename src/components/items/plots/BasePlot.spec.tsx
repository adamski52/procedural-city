import GrassPlot from "./GrassPlot";
import RoadFullIntersection from "../roads/RoadFullIntersection";
// import Item from "../BaseItem";
// import img from "../../../img/Hangar.png";

// class SizeableItem extends Item {
//     constructor(width: number, depth: number) {
//         super({
//             spriteSize: {
//                 width: 100,
//                 depth: 100
//             },
//             spriteSizeCorrection: {
//                 width: 0,
//                 depth: 0
//             },
//             img,
//             isometricSize: {
//                 depth: depth,
//                 width: width
//             },
//             tags: []
//         });
//     }
// }

describe("BasePlot", () => {
    let plot: GrassPlot,
       road = new RoadFullIntersection();

    beforeEach(() => {
        // baseplot is abstract so just pick on one of them.
        plot = new GrassPlot(road);
    });

    it("lol", () => {
        expect(1).toEqual(1);
    });

    // it("should fit an item at 0, 0, but only once", () => {
    //     let item = new SizeableItem(2, 2),
    //         item2 = new SizeableItem(2, 2),
    //         numItems = plot.addItem(item, 0, 0);

    //     expect(numItems).toEqual(1);

    //     numItems = plot.addItem(item2, 0, 0);
    //     expect(numItems).toEqual(1);
    // });

    // it("should fit a 1x1 item at 19, 19", () => {
    //     let item = new SizeableItem(1, 1),
    //         numItems = plot.addItem(item, 19, 19);

    //     expect(numItems).toEqual(1);
    // });

    // it("should not fit a 2x2 item at 19, 19", () => {
    //     let item = new SizeableItem(2, 2),
    //         numItems = plot.addItem(item, 19, 19);

    //     expect(numItems).toEqual(0);
    // });

    // it("should fit a 2x2 item at 18, 18", () => {
    //     let item = new SizeableItem(2, 2),
    //         numItems = plot.addItem(item, 18, 18);

    //     expect(numItems).toEqual(1);
    // });

    // it("should not fit a 3x2 item at 18, 18", () => {
    //     let item = new SizeableItem(3, 2),
    //         numItems = plot.addItem(item, 18, 18);

    //     expect(numItems).toEqual(0);
    // });

    // it("should fit a 3x2 item at 17, 18", () => {
    //     let item = new SizeableItem(3, 2),
    //         numItems = plot.addItem(item, 17, 18);

    //     expect(numItems).toEqual(1);
    // });

    // it("should allow adjacent items as long as they dont overflow the width or overlap", () => {
    //     let numItems = 0,
    //         width = 2,
    //         depth = 1;
        
    //     for(let i = 0; i < 20; i += width) {
    //         let item = new SizeableItem(width, depth);
    //         numItems = plot.addItem(item, i, depth);
    //     }

    //     expect(numItems).toEqual(10);
    // });

    // it("should allow adjacent items as long as they dont overflow the depth or overlap", () => {
    //     let numItems = 0,
    //         width = 1,
    //         depth = 2;
        
    //     for(let i = 0; i < 20; i += depth) {
    //         let item = new SizeableItem(width, depth);
    //         numItems = plot.addItem(item, width, i);
    //     }

    //     expect(numItems).toEqual(10);
    // });

    // it("should allow a full placement", () => {
    //     let item = new SizeableItem(20, 20),
    //         numItems = plot.addItem(item, 0, 0);

    //     expect(numItems).toEqual(1);
    // });

    // it("should allow a multi-item, full placement", () => {
    //     let numItems = 0,
    //         width = 1,
    //         depth = 1;
    
    //     for(let x = 0; x < 20; x += width) {
    //         for(let y = 0; y < 20; y += depth) {
    //             let item = new SizeableItem(width, depth);
    //             numItems = plot.addItem(item, x, y);
    //         }
    //     }

    //     expect(numItems).toEqual(400);
    // });

    // it("should disallow all given a full placement", () => {
    //     let numItems = 0,
    //         width = 1,
    //         depth = 1;
    
    //     for(let x = 0; x < 20; x += width) {
    //         for(let y = 0; y < 20; y += depth) {
    //             let item = new SizeableItem(width, depth);
    //             numItems = plot.addItem(item, x, y);
    //         }
    //     }

    //     for(let x = 0; x < 20; x++) {
    //         for(let y = 0; y < 20; y++) {
    //             let item = new SizeableItem(1, 1);
    //             numItems = plot.addItem(item, x, y);
    //         }
    //     }

    //     expect(numItems).toEqual(400);
    // });
});
