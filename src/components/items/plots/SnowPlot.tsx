import BasePlot from "./BasePlot";
import img from "../../../img/snow.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";
import BaseRoad from "../roads/BaseRoad";

export default class SnowPlot extends BasePlot {
    constructor(road:BaseRoad) {
        super({
            road,
            img,
            tags: [
                TagsEnum.PLOT,
                TagsEnum.SNOW
            ]
        });
    }
}