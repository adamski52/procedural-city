import BasePlot from "./BasePlot";
import img from "../../../img/grass.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";
import BaseRoad from "../roads/BaseRoad";

export default class GrassPlot extends BasePlot {
    constructor(road:BaseRoad) {
        super({
            road,
            img,
            tags: [
                TagsEnum.PLOT,
                TagsEnum.GRASS
            ]
        });
    }
}