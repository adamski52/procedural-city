import BaseItem from "../BaseItem";
import { IPlotProps } from "../../../interfaces/PlotProps";
import IPackedItem from "../../../interfaces/PackedItem";
import BaseRoad from "../roads/BaseRoad";

export default abstract class BasePlot extends BaseItem {
    protected items: BaseItem[] = [];
    protected road: BaseRoad;

    constructor(props: IPlotProps) {
        super({
            spriteSize: {
                width: 754,
                depth: 449
            },
            isometricSize: {
                depth: 20,
                width: 20
            },
            spriteSizeCorrection: {
                width: 0,
                depth: 0
            },
            img: props.img,
            tags: props.tags
        });

        this.road = props.road;

        this.addItem(this.road, 5, 5);
    }

    // private sortItems() {
    //     this.items = this.items.sort((lhs:BaseItem, rhs:BaseItem) => {
    //         let lhsPosition = lhs.getIsometricPosition(),
    //             rhsPosition = rhs.getIsometricPosition();

    //             if(lhsPosition.width === rhsPosition.width) {
    //                 return rhsPosition.depth - lhsPosition.depth;
    //             }
                
    //             return lhsPosition.width - rhsPosition.width;
    //     });
    // }

    private addItem(item: BaseItem, x: number, y: number) {
        item.setIsometricPosition(x, y);
        this.items.push(item);
        this.addChild(item);

        // this.sortItems();
    }

    public addItems(packedItems: IPackedItem[]) {
        packedItems.reverse();
        
        packedItems.forEach((packedItem) => {
            let item = packedItem.item,
                bin = packedItem.bin;

            this.addItem(item, bin.x, bin.y);
        });
    }
}