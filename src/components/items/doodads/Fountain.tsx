import BaseItem from "../BaseItem";
import img from "../../../img/fountain.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class Fountain extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 48,
                depth: 38
            },
            spriteSizeCorrection: {
                width: 1,
                depth: -18
            },
            img,
            isometricSize: {
                depth: 2,
                width: 2
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.FOUNTAIN
            ]
        });
    }
}