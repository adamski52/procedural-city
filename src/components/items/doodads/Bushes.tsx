import BaseItem from "../BaseItem";
import img from "../../../img/bushes.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class Bushes extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 41,
                depth: 32
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.BUSH
            ]
        });
    }
}