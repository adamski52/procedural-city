import BaseItem from "../BaseItem";
import img from "../../../img/boxes.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class Boxes extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 57,
                depth: 49
            },
            spriteSizeCorrection: {
                width: -2,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.JUNK,
                TagsEnum.BOXES
            ]
        });
    }
}