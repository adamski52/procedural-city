import BaseItem from "../BaseItem";
import img from "../../../img/pickup-empty.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class PickupEmpty extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 43,
                depth: 39
            },
            spriteSizeCorrection: {
                width: 9,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.VEHICLE,
                TagsEnum.TRUCK,
                TagsEnum.ALTERNATE
            ]
        });
    }
}