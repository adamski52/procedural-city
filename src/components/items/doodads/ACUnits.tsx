import BaseItem from "../BaseItem";
import img from "../../../img/AC-units.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class ACUnits extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 127,
                depth: 78
            },
            spriteSizeCorrection: {
                width: -24,
                depth: -13
            },
            img,
            isometricSize: {
                depth: 2,
                width: 6
            },
            tags: [
                TagsEnum.JUNK,
                TagsEnum.AC_UNITS
            ]
        });
    }
}