import BaseItem from "../BaseItem";
import img from "../../../img/bushes-tall.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class BushesTall extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 77,
                depth: 59
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.BUSH
            ]
        });
    }
}