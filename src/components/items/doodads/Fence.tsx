import BaseItem from "../BaseItem";
import img from "../../../img/fence.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class Fence extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 152,
                depth: 102
            },
            spriteSizeCorrection: {
                width: -73,
                depth: -14
            },
            img,
            isometricSize: {
                depth: 1,
                width: 9
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.FENCE
            ]
        });
    }
}