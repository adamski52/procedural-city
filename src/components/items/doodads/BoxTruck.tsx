import BaseItem from "../BaseItem";
import img from "../../../img/box-truck.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class BoxTruck extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 127,
                depth: 78
            },
            spriteSizeCorrection: {
                width: 9,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.VEHICLE,
                TagsEnum.TRUCK,
                TagsEnum.FACING
            ]
        });
    }
}