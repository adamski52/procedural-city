import BaseItem from "../BaseItem";
import img from "../../../img/tree-2-branch.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class TreeTwoBranch extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 32,
                depth: 44
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -14
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.TREE,
                TagsEnum.LARGE
            ]
        });
    }
}