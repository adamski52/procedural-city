import BaseItem from "../BaseItem";
import img from "../../../img/rose-bush.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class RoseBush extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 21,
                depth: 18
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -18
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.BUSH
            ]
        });
    }
}