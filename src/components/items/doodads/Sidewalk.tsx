import BaseItem from "../BaseItem";
import img from "../../../img/sidewalk.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class Sidewalk extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 35,
                depth: 20
            },
            spriteSizeCorrection: {
                width: 1,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.SIDEWALK
            ]
        });
    }
}