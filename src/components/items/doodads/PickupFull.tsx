import BaseItem from "../BaseItem";
import img from "../../../img/pickup-full.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class PickupFull extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 46,
                depth: 38
            },
            spriteSizeCorrection: {
                width: 6,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.TRUCK
            ]
        });
    }
}