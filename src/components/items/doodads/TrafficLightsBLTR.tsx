import BaseItem from "../BaseItem";
import img from "../../../img/traffic-lights-BL-TR.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class TrafficLightsBLTR extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 49,
                depth: 88
            },
            spriteSizeCorrection: {
                width: -21,
                depth: -14
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.TRAFFIC_LIGHT,
                TagsEnum.SW
            ]
        });
    }
}