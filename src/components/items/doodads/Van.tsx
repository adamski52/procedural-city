import BaseItem from "../BaseItem";
import img from "../../../img/van.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class Van extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 54,
                depth: 48
            },
            spriteSizeCorrection: {
                width: 8,
                depth: -17
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.VEHICLE,
                TagsEnum.VAN
            ]
        });
    }
}