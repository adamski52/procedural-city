import BaseItem from "../BaseItem";
import img from "../../../img/traffic-tree.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class TrafficTree extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 17,
                depth: 55
            },
            spriteSizeCorrection: {
                width: 1,
                depth: -14
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.TRAFFIC_LIGHT
            ]
        });
    }
}