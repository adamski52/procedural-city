import BaseItem from "../BaseItem";
import img from "../../../img/logs.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class Logs extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 19,
                depth: 17
            },
            spriteSizeCorrection: {
                width: -1,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.JUNK,
                TagsEnum.LOGS
            ]
        });
    }
}