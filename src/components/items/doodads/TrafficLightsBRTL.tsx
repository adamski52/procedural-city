import BaseItem from "../BaseItem";
import img from "../../../img/traffic-lights-BR-TL.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class TrafficLightsBRTL extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 58,
                depth: 67
            },
            spriteSizeCorrection: {
                width: -25,
                depth: -14
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.TRAFFIC_LIGHT,
                TagsEnum.SE
            ]
        });
    }
}