import BaseItem from "../BaseItem";
import img from "../../../img/vent-pipe.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class VentPipe extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 24,
                depth: 32
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -19
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.JUNK,
                TagsEnum.VENT_PIPE
            ]
        });
    }
}