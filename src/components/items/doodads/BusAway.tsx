import BaseItem from "../BaseItem";
import img from "../../../img/bus-away.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class BusAway extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 54,
                depth: 50
            },
            spriteSizeCorrection: {
                width: -6,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.VEHICLE,
                TagsEnum.BUS,
                TagsEnum.AWAY
            ]
        });
    }
}