import BaseItem from "../BaseItem";
import img from "../../../img/cobblestone.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class Cobblestone extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 70,
                depth: 41
            },
            spriteSizeCorrection: {
                width: -1,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.COBBLESTONE
            ]
        });
    }
}