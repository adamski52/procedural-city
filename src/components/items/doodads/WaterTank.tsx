import BaseItem from "../BaseItem";
import img from "../../../img/water-tank.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class WaterTank extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 50,
                depth: 81
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -20
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.JUNK,
                TagsEnum.WATER_TANK
            ]
        });
    }
}