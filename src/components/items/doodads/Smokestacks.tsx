import BaseItem from "../BaseItem";
import img from "../../../img/smoke-stacks.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class Smokestacks extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 161,
                depth: 108
            },
            spriteSizeCorrection: {
                width: -32,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 2,
                width: 6
            },
            tags: [
                TagsEnum.JUNK,
                TagsEnum.SMOKESTACK
            ]
        });
    }
}