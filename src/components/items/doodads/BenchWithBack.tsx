import BaseItem from "../BaseItem";
import img from "../../../img/bench-with-back.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class BenchWithBack extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 19,
                depth: 19
            },
            spriteSizeCorrection: {
                width: 6,
                depth: -14
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.BENCH,
                TagsEnum.ALTERNATE
            ]
        });
    }
}