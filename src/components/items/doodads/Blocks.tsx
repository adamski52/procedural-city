import BaseItem from "../BaseItem";
import img from "../../../img/blocks.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class Blocks extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 25,
                depth: 22
            },
            spriteSizeCorrection: {
                width: -1,
                depth: -14
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.JUNK,
                TagsEnum.BLOCKS
            ]
        });
    }
}