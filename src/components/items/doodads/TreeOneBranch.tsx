import BaseItem from "../BaseItem";
import img from "../../../img/tree-1-branch.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class TreeOneBranch extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 51,
                depth: 67
            },
            spriteSizeCorrection: {
                width: -4,
                depth: -14
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.TREE,
                TagsEnum.LARGE
            ]
        });
    }
}