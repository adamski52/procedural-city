import BaseItem from "../BaseItem";
import img from "../../../img/bench-flat.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class BenchFlat extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 23,
                depth: 18
            },
            spriteSizeCorrection: {
                width: -3,
                depth: -14
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.BENCH
            ]
        });
    }
}