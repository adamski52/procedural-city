import BaseItem from "../BaseItem";
import img from "../../../img/car.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class Car extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 43,
                depth: 36
            },
            spriteSizeCorrection: {
                width: 5,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.VEHICLE,
                TagsEnum.CAR,
                TagsEnum.FACING
            ]
        });
    }
}