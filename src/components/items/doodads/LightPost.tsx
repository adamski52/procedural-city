import BaseItem from "../BaseItem";
import img from "../../../img/light-post.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class LightPost extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 6,
                depth: 29
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -14
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.LIGHT
            ]
        });
    }
}