import BaseItem from "../BaseItem";
import img from "../../../img/topiary.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class Topiary extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 26,
                depth: 49
            },
            spriteSizeCorrection: {
                width: 1,
                depth: -15
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.BUSH
            ]
        });
    }
}