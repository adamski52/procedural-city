import BaseItem from "../BaseItem";
import img from "../../../img/tree-medium.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class TreeMedium extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 35,
                depth: 48
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -14
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.TREE,
                TagsEnum.MEDIUM
            ]
        });
    }
}