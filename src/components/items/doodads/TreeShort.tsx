import BaseItem from "../BaseItem";
import img from "../../../img/tree-short.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class TreeShort extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 29,
                depth: 39
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -14
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.TREE,
                TagsEnum.SMALL
            ]
        });
    }
}