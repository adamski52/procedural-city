import BaseItem from "../BaseItem";
import img from "../../../img/watch-tower.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class WatchTower extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 36,
                depth: 70
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.JUNK,
                TagsEnum.WATCHTOWER
            ]
        });
    }
}