import BaseItem from "../BaseItem";
import img from "../../../img/traffic-lights-TR-BL.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class TrafficLightsTRBL extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 50,
                depth: 60
            },
            spriteSizeCorrection: {
                width: 25,
                depth: -14
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.TRAFFIC_LIGHT,
                TagsEnum.NE
            ]
        });
    }
}