import BaseItem from "../BaseItem";
import img from "../../../img/bushes-short.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";

export default class BushesShort extends BaseItem {
    constructor() {
        super({
            spriteSize: {
                width: 27,
                depth: 21
            },
            spriteSizeCorrection: {
                width: -5,
                depth: -16
            },
            img,
            isometricSize: {
                depth: 1,
                width: 1
            },
            tags: [
                TagsEnum.DECORATION,
                TagsEnum.BUSH
            ]
        });
    }
}