import img from "../../../img/NW-SW.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";
import BaseRoad from "./BaseRoad";

export default class RoadNWSW extends BaseRoad {
    constructor() {
        super({
            spriteSize: {
                width: 269,
                depth: 248
            },
            spriteSizeCorrection: {
                width: -82,
                depth: -8
            },
            img,
            isometricSize: {
                depth: 20,
                width: 20
            },
            tags: [
                TagsEnum.ROAD,
                TagsEnum.NW,
                TagsEnum.SW
            ]
        });
    }
}