import img from "../../../img/x.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";
import BaseRoad from "./BaseRoad";

export default class RoadFullIntersection extends BaseRoad {
    constructor() {
        super({
            spriteSize: {
                width: 432,
                depth: 249
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -8
            },
            isometricSize: {
                depth: 20,
                width: 20
            },
            img,
            tags: [
                TagsEnum.ROAD,
                TagsEnum.NW,
                TagsEnum.NE,
                TagsEnum.SE,
                TagsEnum.SW
            ]
        });
    }
}