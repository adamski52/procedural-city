import img from "../../../img/SE-SW.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";
import BaseRoad from "./BaseRoad";

export default class RoadSESW extends BaseRoad {
    constructor() {
        super({
            spriteSize: {
                width: 430,
                depth: 155
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -8
            },
            img,
            isometricSize: {
                depth: 20,
                width: 20
            },
            tags: [
                TagsEnum.ROAD,
                TagsEnum.SE,
                TagsEnum.SW
            ]
        });
    }
}