import img from "../../../img/NW-SE.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";
import BaseRoad from "./BaseRoad";

export default class RoadNWSE extends BaseRoad {
    constructor() {
        super({
            spriteSize: {
                width: 431,
                depth: 249
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -8
            },
            img,
            isometricSize: {
                depth: 20,
                width: 20
            },
            tags: [
                TagsEnum.ROAD,
                TagsEnum.NW,
                TagsEnum.SE
            ]
        });
    }
}