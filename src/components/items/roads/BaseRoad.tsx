import BaseItem from "../BaseItem";
import { IItemProps } from "../../../interfaces/ItemProps";

export default abstract class BaseRoad extends BaseItem {
    constructor(props:IItemProps) {
        super(props);
    }
}