import BaseItem from "../BaseItem";
import img from "../../../img/NW-NE.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";
import BaseRoad from "./BaseRoad";

export default class RoadNWNE extends BaseRoad {
    constructor() {
        super({
            spriteSize: {
                width: 430,
                depth: 155
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -102
            },
            img,
            isometricSize: {
                depth: 20,
                width: 20
            },
            tags: [
                TagsEnum.ROAD,
                TagsEnum.NW,
                TagsEnum.NE
            ]
        });
    }
}