import img from "../../../img/NE-SE.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";
import BaseRoad from "./BaseRoad";

export default class RoadNESE extends BaseRoad {
    constructor() {
        super({
            spriteSize: {
                width: 268,
                depth: 248
            },
            spriteSizeCorrection: {
                width: 82,
                depth: -8
            },
            isometricSize: {
                depth: 20,
                width: 20
            },
            img,
            tags: [
                TagsEnum.ROAD,
                TagsEnum.NE,
                TagsEnum.SE
            ]
        });
    }
}