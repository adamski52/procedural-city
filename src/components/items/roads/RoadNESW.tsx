import img from "../../../img/NE-SW.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";
import BaseRoad from "./BaseRoad";

export default class RoadNESW extends BaseRoad {
    constructor() {
        super({
            spriteSize: {
                width: 429,
                depth: 248
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -8
            },
            img,
            isometricSize: {
                depth: 20,
                width: 20
            },
            tags: [
                TagsEnum.ROAD,
                TagsEnum.NE,
                TagsEnum.SW
            ]
        });
    }
}