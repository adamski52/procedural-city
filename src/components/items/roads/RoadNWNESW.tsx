import img from "../../../img/NW-NE-SW.png";
import { TagsEnum } from "../../../interfaces/TagsEnum";
import BaseRoad from "./BaseRoad";

export default class RoadNWNESW extends BaseRoad {
    constructor() {
        super({
            spriteSize: {
                width: 432,
                depth: 249
            },
            spriteSizeCorrection: {
                width: 0,
                depth: -8
            },
            img,
            isometricSize: {
                depth: 20,
                width: 20
            },
            tags: [
                TagsEnum.ROAD,
                TagsEnum.NW,
                TagsEnum.NE,
                TagsEnum.SW
            ]
        });
    }
}