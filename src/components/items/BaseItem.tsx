import * as PIXI from "pixi.js";
import { IItemProps, IIsometricSize } from "../../interfaces/ItemProps";
import { TagsEnum } from "../../interfaces/TagsEnum";
import ConstantsService from "../../service/ConstantsService";

export default abstract class BaseItem extends PIXI.Container {
    protected image: PIXI.Texture;
    protected sprite: PIXI.Sprite;
    protected tags: TagsEnum[];

    protected spriteSizeCorrection: IIsometricSize = {
        width: 0,
        depth: 0
    };

    protected spriteSize: IIsometricSize = {
        width: 0,
        depth: 0
    };

    protected isometricSize: IIsometricSize = {
        width: 0,
        depth: 0
    };

    protected isometricPosition: IIsometricSize = {
        width: 0,
        depth: 0
    };

    constructor(props: IItemProps) {
        super();

        this.spriteSize = props.spriteSize;

        this.spriteSizeCorrection = props.spriteSizeCorrection;

        this.tags = props.tags || [];

        this.isometricSize = {
            width: props.isometricSize.width + 2,
            depth: props.isometricSize.depth + 2
        };

        this.image = PIXI.Texture.from(props.img);
        this.sprite = new PIXI.Sprite(this.image);
        this.sprite.anchor.x = .5;
        this.sprite.anchor.y = 1;
        this.addChild(this.sprite);
    }

    public setIsometricPosition(xSlot: number, ySlot: number) {
        this.isometricPosition = {
            width: xSlot,
            depth: ySlot
        };

        let scaledX = this.isometricPosition.width * ConstantsService.ISO_SLOT_SIZE,
            scaledY = this.isometricPosition.depth * ConstantsService.ISO_SLOT_SIZE,
            isoX = scaledX - scaledY,
            isoY = (scaledX + scaledY) / ConstantsService.ISO_RATIO;

        this.setPosition(isoX, isoY);
    }

    private setPosition(x: number, y: number) {
        this.x = (this.spriteSizeCorrection.width + x);
        this.y = (this.spriteSizeCorrection.depth - y);
    }

    public flip() {
        let isometricSize = this.getIsometricSize();

        this.isometricSize = {
            width: isometricSize.depth,
            depth: isometricSize.width
        };

        this.scale.x = -1;

        let spriteSizeCorrection = this.getSpriteSizeCorrection();

        this.spriteSizeCorrection = {
            width: -spriteSizeCorrection.width,
            depth: spriteSizeCorrection.depth
        };
    }

    public getIsometricPosition() {
        return this.isometricPosition;
    }

    public getIsometricSize() {
        return this.isometricSize;
    }

    public getSpriteSizeCorrection() {
        return this.spriteSizeCorrection;
    }

    public getSpriteSize() {
        return this.spriteSize;
    }

    public getTags() {
        return this.tags;
    }
}