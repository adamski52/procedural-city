import React from 'react';
import Stage from './Stage';

export default class App extends React.Component<any, any> {

    private onReload() {
        window.location.reload();
    }

    public render() {
        return (
            <div className="App">
                <p>Note that this is incomplete and has many bugs, especially related to road placement.  Reload to see another random run.</p>
                <button onClick={this.onReload}>Reload</button>
                <Stage />
            </div>
        );
    }
}