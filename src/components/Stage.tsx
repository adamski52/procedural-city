import React from 'react';
import * as PIXI from "pixi.js";
import SnowPlot from './items/plots/SnowPlot';
import PackingService from "../service/PackingService";
import ItemsService from '../service/ItemsService';
import { TagsEnum } from "../interfaces/TagsEnum";
import PlotService from '../service/PlotService';

export default class Stage extends React.Component<any, any> {
    private app: PIXI.Application;

    constructor(props: any) {
        super(props);
        this.app = new PIXI.Application({
            width: window.innerWidth,
            height: window.innerHeight,
            backgroundColor: 0x000000,
            // resolution: window.devicePixelRatio || 1
            resolution: 1
        });

        let grass = PlotService.getPlot("snow", undefined, undefined),
            spriteSize = grass.getSpriteSize();

        grass.x = spriteSize.width / 2;
        grass.y = spriteSize.depth;

        let items = [...ItemsService.getItemsByTags([TagsEnum.COMMERCIAL]), ...ItemsService.getItemsByTags([TagsEnum.OFFICE])];

        let packedItems = PackingService.packItems(20, 20, items);

        grass.addItems(packedItems);

        this.app.stage.addChild(grass);
    }

    public render() {
        return (
            <div id="stage" />
        );
    }

    public componentDidMount() {
        document.getElementById("stage")?.appendChild(this.app.view);
    }
}