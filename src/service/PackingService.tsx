import ShelfPack from "@mapbox/shelf-pack";
import BaseItem from "../components/items/BaseItem";
import IPackedItem from "../interfaces/PackedItem";

export default class PackingService {

    private static shuffle(items: BaseItem[]) {
        let tempValue,
            randomIndex;
          
        for(let i = 0; i < items.length; i++) {
            randomIndex = Math.floor(Math.random() * i);
            
            tempValue = items[i];
            items[i] = items[randomIndex];
            items[randomIndex] = tempValue;
        }
        
        return items;
    }

    public static packItems(containerWidth: number, containerDepth: number, items: BaseItem[], shuffle: boolean = true) {
        if(shuffle) {
            items = this.shuffle(items);
        }

        let itemSize,
            bin,
            placedItems:IPackedItem[] = [],
            container = new ShelfPack(containerWidth, containerDepth, {
                autoResize: false
            });

        items.forEach((item) => {
            itemSize = item.getIsometricSize();
            bin = container.packOne(itemSize.depth, itemSize.width); // bin packer's axes are reversed of ours

            if(!bin) {
                console.log("item didnt fit", item);
                return;
            }

            placedItems.push({
                item,
                bin
            });
        });

        return placedItems;
    }
}