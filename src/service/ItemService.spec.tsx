import ItemsService from './ItemsService';
import { TagsEnum } from "../interfaces/TagsEnum";
describe("ItemsService", () => {
    it("getItemsByTags should return all items if no meta specified", () => {
        let actual = ItemsService.getItemsByTags([]);
        expect(actual.length).toEqual(116);
    });

    it("getItemsByTags should return all items matching a meta's key", () => {
        let actual = ItemsService.getItemsByTags([TagsEnum.JUNK]);
        expect(actual.length).toEqual(8);

        actual = ItemsService.getItemsByTags([TagsEnum.RESIDENTIAL]);
        expect(actual.length).toEqual(16);
    });
});
