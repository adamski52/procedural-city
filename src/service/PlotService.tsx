import BaseItem from "../components/items/BaseItem";
import SnowPlot from "../components/items/plots/SnowPlot";
import GrassPlot from "../components/items/plots/GrassPlot";
import CementPlot from "../components/items/plots/CementPlot";
import RoadNESE from "../components/items/roads/RoadNESE";
import RoadFullIntersection from "../components/items/roads/RoadFullIntersection";

export default class PlotService {
    public static getPlot(theme: string, southernNeighbor: BaseItem | undefined, westernNeighbor: BaseItem | undefined) {
        let road = new RoadFullIntersection();
        if(southernNeighbor === undefined) {
            if(westernNeighbor === undefined) {
                road = new RoadNESE();
            }
        }

        let plot;
        switch(theme) {
            case "snow":
                plot = new SnowPlot(road);
                break;
            case "cement":
                plot = new CementPlot(road);
                break;
            default:
                plot = new GrassPlot(road);
                break;
        }

        return plot;
    }
}