import BaseItem from "../components/items/BaseItem";
import { Bin } from "@mapbox/shelf-pack";

export default interface IPackedItem {
    item: BaseItem;
    bin: Bin;
}