import { TagsEnum } from "../interfaces/TagsEnum";
import BaseRoad from "../components/items/roads/BaseRoad";

export interface IPlotProps {
    img: string;
    road: BaseRoad;
    tags?: TagsEnum[];
}