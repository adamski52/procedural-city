import { TagsEnum } from "../interfaces/TagsEnum";

export interface IIsometricSize {
    depth: number;
    width: number;
}

export interface IItemProps {
    spriteSize: IIsometricSize;
    spriteSizeCorrection: IIsometricSize;
    img: string;
    isometricSize: IIsometricSize;
    tags?: TagsEnum[];
}